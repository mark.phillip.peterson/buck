
/*
 ##################################################################
 ##################################################################
 ######### The servers side of a multiplayer buck game ############
 ### A (very) hacky effort by Mark Peterson started 2014 May 26 ###
 ##################################################################
 ##################################################################
*/


var portToUse = 8000;

var app = require('http').createServer(handler),
	io = require('socket.io').listen(app),
	fs = require('fs');

// Turn on the listening
app.listen(portToUse);


// Start server

function handler (req, res) {
	fs.readFile(__dirname + '/playBuckServer.html',
	function (err, data) {
		if (err) {
			res.writeHead(500);
			return res.end('Error loading playBuck.html');
		}

		res.writeHead(200);
		res.end(data);
	});
}



// set namespace:
var BUCK = {
	// A variable to hold a separate connections for each player
	playerSockets : {
			p1: null,
			p2: null,
			p3: null,
			p4: null
	},
	
	// Hold info on each player -- may be usurped by the get/set in sockets
	playerInfo : {
		p1: null,
		p2: null,
		p3: null,
		p4: null
	},
	
	// for tracking progress
	score: {
		aGame: 0,
		bGame: 0,
		aHand: 0,
		bHand: 0
	},

	// for tracking current hand
	currHand: {
		trump: null,
		bidder: null,
		bid: null
	},
	
	// =============================== //
	// TODO: grab team name from login //
	// =============================== //
	team: {
		a: "teamA", // team with 1 and 3
		b: "teamB"  // team with 2 and 4
	},
	
	// Set Deck information
	suits : ["C","D","H","S"],
	cardLev : ["9","T","J","Q","K","A"], //Use a "T" for 10 to keep all same size
	deck : [], // built below:
		
	// Function to get available slots for sign in:
	getAvailSlots : function(){
		var tmpPlayers = [];
		for(var i = 1; i < 5; i++){
			if(BUCK.playerSockets['p'+i]){
				BUCK.playerSockets['p'+i].get('playerName',function(err,name){
					tmpPlayers[i-1] = "Player " + i + ": " + name;
				});
			} else{
				tmpPlayers[i-1] = "Player " + i + ": " + "available";
			}
		}
		return tmpPlayers;
	},

	// init function to hold anything I need to set at game start -- currently run right away
	init : function() {
		// Build deck
		for(var i = 0; i < BUCK.suits.length; i++){
			for(var k = 0; k < BUCK.cardLev.length; k++){
				BUCK.deck.push(BUCK.cardLev[k] + "" + BUCK.suits[i]); 
			}
		}
	},
	
	
	// function to start a new game; could call init
	startGame :  function() {
			
		// Set team names
		BUCK.team.a = BUCK.playerInfo.p1.split("").slice(0,8).join("") + "/" + BUCK.playerInfo.p3.split("").slice(0,8).join("");
		BUCK.team.b = BUCK.playerInfo.p2.split("").slice(0,8).join("") + "/" + BUCK.playerInfo.p4.split("").slice(0,8).join("");
		BUCK.allPlayers.emit("gameStart",{team : BUCK.team});
		
		// Clear score
		BUCK.score = {
			aGame: 0,
			bGame: 0,
			aHand: 0,
			bHand: 0
		}
	}
	

}

BUCK.init();



	


BUCK.allPlayers = io
		.of('/allBuckPlayers')
		.on('connection', function (socket) {
			// Determine which users exist
			var tmpPlayers = BUCK.getAvailSlots();
			
			socket.emit('pickPos',{info: 'Which player position would you like?\n(choose a number that is available or 5 to cancel)\n\n' +
											tmpPlayers.join("\n")});
											
			socket.on(
				'posPicked',function( data){
				console.log("They Picked: " + data.pos)
				//	console.log("socket info: " + socket)
					if(BUCK.playerSockets["p" + data.pos]){
						// deal with error if already set
						var tmpPlayers = BUCK.getAvailSlots();
						
						socket.emit('pickPos',
									{info: 'It appears that your selection was already taken\n\n' +
									'Which player position would you like?\n(choose a number that is available or 5 to cancel)\n\n' +
											tmpPlayers.join("\n")});
					
					} else {
						if( data.pos < 5 && data.pos > 0  && isInt(data.pos) ){


							// Set the player socket position	
							BUCK.playerSockets["p" + data.pos] = socket;
							BUCK.playerSockets["p" + data.pos].set('playerNumber', data.pos);
							BUCK.playerSockets["p" + data.pos].emit('pickName',
														  {info: 'What screen name would you like?'});
							
														  
							// Set the function to get/set the player's name on the return packet
							BUCK.playerSockets["p" + data.pos].on('namePicked',function(data){
								socket.set('playerName', data.username);

								socket.get('playerNumber',function(err,name){
									console.log("Position " + name  + " is taken by user: " + data.username);
									
									BUCK.playerInfo["p"+name] = data.username;	
									
									socket.broadcast.emit("infoMessage",
    													  {info: "Position " + name  + " is: " + data.username});
								});
								
								
								// Check to see if this is the last player
								var tmpPlayers = BUCK.getAvailSlots();
								var patt = new RegExp("available");
								var availSlots = 0;
								for(var k = 0;k<tmpPlayers.length;k++){
									availSlots += 0 + patt.test(tmpPlayers[k]);
								}
								
								if(availSlots == 0){
									BUCK.startGame();
								} else {
									BUCK.allPlayers.emit("infoMessage",
														 {info:"     Still waiting for\n" +
														       "          "+ availSlots + " players"});
								}

								
								// THIS IS JUST FOR TESTING
								// TODO remove this
								socket.emit('newHand',
											 {hand : ["9C","TD","JH","QS","KS","AD"]});
								
								
							});
							
							/************************************************\
							 ** Set all of the other needed functions here **
							\************************************************/
							
							// e.g. play card, deal, disconnect, etc.
							
							
							
							
							
							
							

						} else if (data.pos == 5) {
							// Let connection die if they cancelled out
							
						
						} else {


							// Deal with errors if data.pos is out of range
							var tmpPlayers = BUCK.getAvailSlots();

							socket.emit('pickPos',
										{info: 'It appears that your selection was out of range\n\n' +
										'Which player position would you like?\n(choose a number that is available or 5 to cancel)\n\n' +
												tmpPlayers.join("\n")});
					


						}
					}
				});
		});


function isInt(x) {
  var y=parseInt(x);
  if (isNaN(y)) return false;
  return x==y && x.toString()==y.toString();
}


